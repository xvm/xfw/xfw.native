"""
This file is part of the XVM Framework project.

Copyright (c) 2017-2022 XVM Team.

This file is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

__ALL__ = [
    'is_native_available'
    'load_native'
    'unpack_native'
    'unpack_load_native'
    'XFWNativeModuleWrapper'
]

# CPython
import logging
import imp
import platform
import os
import os.path

# XFW
import xfw_loader.python as loader
import openwg_vfs as vfs



#
# Globals
#

__xfwnative_available = False
__xfwnative_python27 = None
__xfwnative_helper = None



#
# Private
#

def __get_subfolder():
    if loader.get_client_realm() == 'RU':
        return 'native_lesta'
    return 'native_wg'


def __get_origin_path(package_id, subdir = None):
    mod_path = loader.get_mod_directory_path(package_id)

    if not subdir:
        subdir = __get_subfolder()

    return u'%s/%s' % (mod_path, subdir)


def __get_realfs_path(package_id, subdir = None):
    if loader.is_mod_in_realfs(package_id):
        return __get_origin_path(package_id, subdir)

    if not subdir:
        subdir = __get_subfolder()

    return u'%s/%s/%s' % (loader.XFWLOADER_TEMPDIR, package_id, subdir)


def __load_native(module_name, module_folder, module_filename):
    result = None
    logger = logging.getLogger('XFW/Native')

    module_path = './' + os.path.join(module_folder, module_filename).replace('\\', '/')
    logger.debug(u"[__load_native] Trying to load module: module_name='%s', module_path='%s'" % (unicode(module_name), unicode(module_path)))

    try:
        result = imp.load_dynamic(module_name, module_path.encode('mbcs'))
    except ImportError as exc:
        logger.exception(u"[__load_native] Error on loading native on try 1: module_name='%s', module_path='%s'" % (unicode(module_name), unicode(module_path)))

    return result



#
# Publics
#

def is_native_available():
    """
    Returns True if native components are successfuly loaded
    """
    return __xfwnative_available


def load_native(package_id, library_filename, module_name):
    """
    Tries to load native module and return module object
    arguments:
      * package_id: package ID defined in xfw_package.json
      * library_filename: name of dll file with extension
      * module_name: nome of module to import
    returns:
      * module object or None on error
    """

    return __load_native(module_name, __get_realfs_path(package_id), library_filename)


def unpack_native(package_id, subdir = None):
    """
    Unpacks native files for XFW package
    arguments:
        * package_id: package ID defined in xfw_package.json
        * custom_subdir: use custom subdirectory instead of native_{32,64}bit for unpacking
    returns:
        * path to unpacked files
        * None on error
    """

    logger = logging.getLogger('XFW/Native')

    # check if package is loaded
    if not loader.is_mod_exists(package_id):
        logger.warning("[unpack_native] mod '%s' is not exists" % package_id)
        return None

    path_origin = __get_origin_path(package_id, subdir)
    if not path_origin:
        logger.warning("[unpack_native] failed to get origin path for package '%s'" % package_id)
        return None

    path_realfs = __get_realfs_path(package_id, subdir)
    if not path_realfs:
        logger.warning("[unpack_native] failed to calculate realfs path for package '%s'" % package_id)
        return None

    # check paths for validity
    if loader.is_mod_in_realfs(package_id):
        if not os.path.exists(path_origin):
            logger.warning("[unpack_native] there is no RealFS files to load for package '%s' and architecture '%s' and subdir '%s'" % (package_id, platform.architecture()[0], subdir))
            return None
    else:
        if not vfs.directory_exists(path_origin):
            logger.warning("[unpack_native] there is no VFS files to load for package '%s' and architecture '%s' and subdir '%s'" % (package_id, platform.architecture()[0], subdir))
            return None

    # unpack from VFS
    vfs.directory_copy(path_origin, path_realfs)

    # add to PATH variable
    if __xfwnative_helper is not None and __xfwnative_helper.xfwnative_wrapper_initialized():
        __xfwnative_helper.path_prepend(unicode(os.path.abspath(path_realfs)))

    return path_realfs


def unpack_load_native(package_id, library_filename, module_name):
    """
    Tries to unpack and load native module and return module object
    arguments:
      * package_id: package ID defined in xfw_package.json
      * library_filename: name of dll file with extension
      * module_name: nome of module to import
    returns:
      * module object or None on error
    """

    logger = logging.getLogger('XFW/Native')
    try:
        if not unpack_native(package_id):
            logger.error('[unpack_load_native] Failed to load native module. Failed to unpack native module %s' % package_id)
            return None

        result = load_native(package_id, library_filename, module_name)
        if not result:
            logger.error("[unpack_load_native] Failed to load native module %s,%s,%s" % (package_id, library_filename, module_name))
            return None

        return result

    except Exception:
        logger.exception('[unpack_load_native] Error when loading native library:')



#
# XFW Native Module Wrapper
#

class XFWNativeModuleWrapper(object):
    def __init__(self, package, file, module):
        self.__initialized = False
        self.__native = None

        self.__name_package = package
        self.__name_file = file
        self.__name_module = module

        try:
            if not is_native_available():
                logging.getLogger('XFW/Native/Wrapper').error('XFW native is not ready')
                return
    
            self.__native = unpack_load_native(self.__name_package, self.__name_file, self.__name_module)
            if self.__native is None:
                logging.getLogger('XFW/Native/Wrapper').error("Failed to load native module: package = %s, file = %s, module = %s" % (self.__name_package, self.__name_file, self.__name_module))
                return

            self.__initialized = True

        except Exception:
            logging.getLogger('XFW/Native/Wrapper').exception("exception while loading native library:")


    # XFW functions
    def xfwnative_wrapper_initialized(self):
        return self.__initialized

    def xfwnative_wrapper_name_package(self):
        return self.__name_package

    def xfwnative_wrapper_name_file(self):
        return self.__name_file

    def xfwnative_wrapper_name_module(self):
        return self.__name_module

    def xfwnative_wrapper_object_module(self):
        return self.__native

    # Magic functions
    def __delattr__(self, attr):
        if self.__initialized:
            return delattr(self.__native, attr)
        return object.__delattr__(self, attr)

    def __getattribute__(self, attr):
        if attr.startswith('xfwnative') or attr.startswith('_XFWNativeModuleWrapper'):
            return object.__getattribute__(self, attr)
        if object.__getattribute__(self, '_XFWNativeModuleWrapper__initialized'):
            return getattr(self.__native, attr)
        return object.__getattribute__(self, attr)

    def __setattr__(self, attr, value):
        if attr == '_XFWNativeModuleWrapper__initialized':
            return object.__setattr__(self, attr, value)
        if self.__initialized:
            return setattr(self.__native, attr, value)
        return object.__setattr__(self, attr, value)

    def __repr__(self):
        if self.__initialized:
            return repr(self.__native)
        return object.__repr__(self)

    def __str__(self):
        if self.__initialized:
            return str(self.__native)
        return object.__str__(self)



#
# XFW
#

def xfw_is_module_loaded():
    return is_native_available()


def xfw_module_init():
    global __xfwnative_available
    global __xfwnative_python27
    global __xfwnative_helper

    logger = logging.getLogger('XFW/Native')

    # prevent double initialization
    if __xfwnative_available or __xfwnative_helper is not None:
        logger.warning('init: double initialization')
        return

    # 32-bit is not supported
    if platform.architecture()[0] != "64bit":
        logger.warning("init: 32-bit client is not supported")
        return

    if loader.get_client_realm() == 'RU':
        logger.warning('\n' +
                       '!!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!  \n'
                       '!!!   init: Native modules support is limited in Mir Tankov since v1.30.0 due to changes by Lesta Games\n'
                       '!!!         For additional information, you can: \n'
                       '!!!             * read the discussion on kr.cm            : https://koreanrandom.com/forum/topic/84379- \n'
                       '!!!             * check the Lesta Games official channels : https://tanki.su/ru/community/ \n'
                       '!!!             * reach out to game support               : https://lesta.ru/support/ru/products/wot/help/ \n'
                       '!!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!   !!!\n')   

    __xfwnative_available = True

    # load python27 for Lesta
    if loader.get_client_realm() == 'RU':
        __xfwnative_python27 = XFWNativeModuleWrapper('com.modxvm.xfw.native', 'python27.dll', 'python27')
        if __xfwnative_python27 is None:
            __xfwnative_available = False
            logger.error('init: failed to load python27.dll')
            return
        if not __xfwnative_python27.xfwnative_wrapper_initialized():
            logger.error('init: failed to load python27.dll')
            __xfwnative_available = False
            __xfwnative_python27 = None
            return

    # load helper
    __xfwnative_helper = XFWNativeModuleWrapper('com.modxvm.xfw.native', 'xfw_native.pyd', 'XFW_Native')
    if __xfwnative_helper is None:
        __xfwnative_available = False
        logger.error('init: failed to load xfw_native.pyd')
        return
    if not __xfwnative_helper.xfwnative_wrapper_initialized():
        logger.error('init: failed to load xfw_native.pyd')
        __xfwnative_available = False
        __xfwnative_helper = None
        return

    __xfwnative_helper.init()
    __xfwnative_available = True
