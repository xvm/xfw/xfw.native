# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2022-2024 XVM Contributors

#
# Project
#

cmake_minimum_required (VERSION 3.23)
project(xfw_native)



#
# Packages
#

set(PYBIND11_NOPYTHON ON)

find_package(pybind11 REQUIRED)


#
# Library
#

function(add_xfw_library vendor)
    find_package(libpython_${vendor} REQUIRED)

    add_library(xfw_native_${vendor} SHARED)

    target_sources(xfw_native_${vendor} PRIVATE "python_module.cpp")

    target_compile_definitions(xfw_native_${vendor} PRIVATE "NOMINMAX")
    target_compile_definitions(xfw_native_${vendor} PRIVATE "_USE_MATH_DEFINES")
    target_compile_definitions(xfw_native_${vendor} PRIVATE "_CRT_SECURE_NO_WARNINGS")  

    target_link_libraries(xfw_native_${vendor} PRIVATE libpython_${vendor}::libpython_${vendor})
    target_link_libraries(xfw_native_${vendor} PRIVATE pybind11::pybind11)

    set_target_properties(xfw_native_${vendor} PROPERTIES CXX_STANDARD 23)
    set_target_properties(xfw_native_${vendor} PROPERTIES CXX_STANDARD_REQUIRED ON)
    set_target_properties(xfw_native_${vendor} PROPERTIES COMPILE_FLAGS "/wd4005 /wd5033")
    set_target_properties(xfw_native_${vendor} PROPERTIES SUFFIX ".pyd")
    set_target_properties(xfw_native_${vendor} PROPERTIES OUTPUT_NAME "xfw_native")
    set_target_properties(xfw_native_${vendor} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY "${vendor}")
    set_target_properties(xfw_native_${vendor} PROPERTIES LIBRARY_OUTPUT_DIRECTORY "${vendor}")
    set_target_properties(xfw_native_${vendor} PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${vendor}")

    install(
      TARGETS xfw_native_${vendor}
      RUNTIME DESTINATION bin_${vendor}
    )
endfunction()

add_xfw_library(lesta)
add_xfw_library(wg)
