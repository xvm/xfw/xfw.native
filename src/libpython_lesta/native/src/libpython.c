/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2021 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#define Py_BUILD_CORE 1

#include <Windows.h>
#include <Python.h>

#include "libpython.h"
#include "libpython_database.h"
#include "libpython_sysobject.h"
#include "libpython_winapi.h"

HMODULE PyWin_DLLhModule = NULL;

char* PyWin_DLLVersionString = "2.7";

BOOL WINAPI DllMain(IN HINSTANCE hDllHandle, IN DWORD nReason, IN LPVOID Reserved)
{
    switch ( nReason )
    {
        case DLL_PROCESS_ATTACH:
            PyWin_DLLhModule = hDllHandle;
            break;
        case DLL_PROCESS_DETACH:
            if(!xfwnative_winapi_disable())
                return FALSE;
            break;
        default:
            break;
    }

    return TRUE;
}

PyMethodDef python27Methods[] = {
    { 0, 0, 0, 0 }
};

__declspec(dllexport) void initpython27(void)
{
    if (!xfwnative_database_initialize())
        return;

    if (!xfwnative_sysobject_fix())
        return;

    if (!xfwnative_winapi_enable())
        return;

    Py_InitModule("python27", python27Methods);
}
