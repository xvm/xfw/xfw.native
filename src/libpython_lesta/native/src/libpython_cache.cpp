/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2021 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <filesystem>
#include <fstream>
#include <vector>
#include <sstream>


#include "libpython_cache.h"
#include "libpython_hash.h"
#include "libpython_path.h"

#include "libpython.h"
#include "xfwnative_autogen_func.h"
#include "xfwnative_autogen_signature.h"
#include "xfwnative_autogen_struct.h"

static inline std::filesystem::path search_directory = L"../mods/temp/com.modxvm.xfw.native/cache/";

bool xfwnative_cache_deserialize() {
    wchar_t* path_app = xfwnative_path_getapplicationpath();
    wchar_t* path_dll = xfwnative_path_getdllpath();

    uint64_t hash_app = xfwnative_hash_calculateforfile(path_app);
    uint64_t hash_dll = xfwnative_hash_calculateforfile(path_dll);

    auto cache_dir = std::filesystem::path(path_app).parent_path() / search_directory;

    free(path_app);
    free(path_dll);

    if (!std::filesystem::exists(cache_dir)) {
        return false;
    }

    for (auto& filepath : std::filesystem::directory_iterator(cache_dir))
    {
        std::ifstream file(filepath.path(), std::ios::in | std::ios::binary);

        uint64_t hash_app_f = 0;
        uint64_t hash_dll_f = 0;
       
        file.read(reinterpret_cast<char*>(&hash_app_f), sizeof(hash_app_f));
        if (hash_app != hash_app_f) {
            file.close();
            continue;
        }
       
        file.read(reinterpret_cast<char*>(&hash_dll_f), sizeof(hash_dll_f));
        if (hash_dll != hash_dll_f) {
            file.close();
            continue;
        }

        file.read(reinterpret_cast<char*>(Functions_Addrs), Functions_Count * sizeof(size_t));
        file.read(reinterpret_cast<char*>(xfwnative_autogen_struct_addrs), Structures_Count * sizeof(size_t));

        file.close();

        xfwnative_autogen_struct_update();
        return true;
    }

    return false;
}

void xfwnative_cache_serialize()
{
    std::wostringstream oss;

    wchar_t* path_app = xfwnative_path_getapplicationpath();
    wchar_t* path_dll = xfwnative_path_getdllpath();

    uint64_t hash_app = xfwnative_hash_calculateforfile(path_app);
    uint64_t hash_dll = xfwnative_hash_calculateforfile(path_dll);


    for (size_t i = 0; i < sizeof(hash_app); i++) {
        oss << std::hex << std::setw(2) << std::setfill(L'0') << reinterpret_cast<uint8_t*>(&hash_app)[i];
    }

    for (size_t i = 0; i < sizeof(hash_dll); i++) {
        oss << std::hex << std::setw(2) << std::setfill(L'0') << reinterpret_cast<uint8_t*>(&hash_dll)[i];
    }

    auto filepath = std::filesystem::path(path_app).parent_path() / search_directory / oss.str();

    if (!std::filesystem::exists(filepath.parent_path().parent_path().parent_path().parent_path().parent_path()))
        std::filesystem::create_directory(filepath.parent_path().parent_path().parent_path().parent_path().parent_path());

    if (!std::filesystem::exists(filepath.parent_path().parent_path().parent_path().parent_path()))
        std::filesystem::create_directory(filepath.parent_path().parent_path().parent_path().parent_path());

    if (!std::filesystem::exists(filepath.parent_path().parent_path().parent_path()))
        std::filesystem::create_directory(filepath.parent_path().parent_path().parent_path());

    if (!std::filesystem::exists(filepath.parent_path().parent_path()))
        std::filesystem::create_directory(filepath.parent_path().parent_path());

    if (!std::filesystem::exists(filepath.parent_path()))
        std::filesystem::create_directory(filepath.parent_path());

    std::ofstream file(filepath, std::ios::out | std::ios::binary);
    file.write(reinterpret_cast<char*>(&hash_app), sizeof(hash_app));
    file.write(reinterpret_cast<char*>(&hash_dll), sizeof(hash_dll));

    file.write(reinterpret_cast<char*>(Functions_Addrs), Functions_Count * sizeof(size_t));
   
    file.write(reinterpret_cast<char*>(xfwnative_autogen_struct_addrs), Structures_Count * sizeof(size_t));
   
    file.close();

    free(path_app);
    free(path_dll);
}
