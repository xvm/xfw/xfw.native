#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool xfwnative_memory_datacompare(const char* pData, const char* bMask, const char* szMask);

size_t xfwnative_memory_findfunction(size_t startpos, size_t endpos, const char* pattern, const char* mask);

size_t xfwnative_memory_getmodulesize(const wchar_t* filename);

uint32_t xfwnative_memory_read32bit(size_t address);

bool xfwnative_memory_validatestructure(size_t address, const char* validatePattern, const char* validateMask);

#ifdef __cplusplus
}
#endif
