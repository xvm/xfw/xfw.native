#include <Windows.h>
#include <Tlhelp32.h>
#include <process.h>

#include "libpython_memory.h"

bool xfwnative_memory_datacompare(const char* pData, const char* bMask, const char* szMask)
{
    for (; *szMask; ++szMask, ++pData, ++bMask) {
        if (*szMask == 'x' && *pData != *bMask) {
            return false;
        }
    }

    return true;
}


size_t xfwnative_memory_findfunction(size_t startpos, size_t endpos, const char* pattern, const char* mask)
{
    for (size_t pos = startpos; pos < endpos; pos++) {
        if (xfwnative_memory_datacompare((const char*)(pos), pattern, mask)) {
            return pos;
        }
    }

    return 0;
}


size_t xfwnative_memory_getmodulesize(const wchar_t* filename)
{
    MODULEENTRY32W moduleEntry;
    size_t moduleSize = 0;

    HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, _getpid());
    if (hSnap == NULL) {
        return moduleSize;
    }

    moduleEntry.dwSize = sizeof(MODULEENTRY32W);

    BOOL result = Module32FirstW(hSnap, &moduleEntry);
    while (result)
    {
        if (wcsstr(filename, moduleEntry.szModule))
        {
            moduleSize = moduleEntry.modBaseSize;
            break;
        }

        result = Module32NextW(hSnap, &moduleEntry);
    }

    CloseHandle(hSnap);

    return moduleSize;
}


uint32_t xfwnative_memory_read32bit(size_t address)
{
    const char* memory = (const char*)(address);

    return MAKELONG(
        MAKEWORD(memory[0], memory[1]),
        MAKEWORD(memory[2], memory[3])
    );
}


bool xfwnative_memory_validatestructure(size_t address, const char* validatePattern, const char* validateMask)
{
    return xfwnative_memory_datacompare((const char*)(address - strlen(validateMask)), validatePattern, validateMask);
}
