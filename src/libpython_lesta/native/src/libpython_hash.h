#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

uint64_t xfwnative_hash_calculateforfile(const wchar_t* filepath);

#ifdef __cplusplus
}
#endif
