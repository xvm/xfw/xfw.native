﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

using XFWNative.Libpython.Common;
using XFWNative.Libpython.Common.Data.Signatures;

namespace XFWNative.Libpython.Codegen
{
    public class CodeGenerator
    {

        private DB db { get; set; } = null;

        private string templateDir { get; set; } = null;

        private string outputDir { get; set; } = null;

        private Dictionary<string,StringBuilder> macroses { get;} = new Dictionary<string, StringBuilder>();

        public CodeGenerator(string dataDir, string templateDir, string outputDir)
        {
            db = new DB(dataDir);

            this.templateDir = templateDir;
            this.outputDir = outputDir;


            processAsm();
            ProcessFunctions();
            ProcessStructures();
        }


        void appendMacroLine(string macro, string text)
        {
            if (!macroses.ContainsKey(macro))
                macroses[macro] = new StringBuilder();

            macroses[macro].AppendLine(text);
        }


        void appendMacro(string macro, string text)
        {
            if (!macroses.ContainsKey(macro))
                macroses[macro] = new StringBuilder();

            macroses[macro].Append(text);
        }


        void processAsm()
        { 
            appendMacro("MASM_POINTER_TYPE", "QWORD");
            appendMacroLine("MASM_MODEL", "");
        }

        void ProcessFunctions()
        {
            foreach (var rec in db.GetFunctions().Where(x => x.IsInternal))
            {
                appendMacroLine("FUNCTIONS_INTERNAL_DECLARATION", $"{rec.ReturnType} {rec.Name}({rec.Arguments});");
            }

            int functionIndex = -1;
            int maxSignaturesCount = 0;

            foreach (var func in db.GetFunctions())
            {  
                //update max signature counter
                if (maxSignaturesCount < func.Signatures.Count)
                {
                    maxSignaturesCount = func.Signatures.Count;
                }

                if (func.Signatures.Count == 0)
                    continue;

                if (func.Inlined == true)
                    continue;

                functionIndex++;

                appendMacroLine("FUNCTIONS_NAMES", $"    funcenum_{func.Name}={functionIndex},");

                appendMacroLine("FUNCTIONS_INITIALIZE_NAMES", $"    \"{func.Name}\",");
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_COUNT", $"    {func.Signatures.Count},");

                //fill signature array
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_DATA", "    {");
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_MASK", "    {");
                foreach (var signature in func.Signatures)
                {
                    (var pattern, var mask) = Helper.ProcessSignature(signature.Signature);
                    appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_DATA", $"        \"{pattern}\",");
                    appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_MASK", $"        \"{mask}\",");
                }
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_DATA", "    },");
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_MASK", "    },");

                //fill trampoline
                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"{func.Name} PROC EXPORT");

                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"    jmp Functions_Addrs[{functionIndex}*8]");

                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"{func.Name} ENDP");
                appendMacroLine("FUNCTIONS_TRAMPOLINES", "");

            }


            appendMacro("FUNCTIONS_SIGNATURES_MAXCOUNT", maxSignaturesCount.ToString());
            appendMacro("FUNCTIONS_COUNT", (functionIndex + 1).ToString());

        }

        void ProcessStructures()
        {

            int structureIndex = -1;
            int maxSignaturesCount = 0;

            foreach (var structure in db.GetStructures())
            {
                if (structure.Signatures.Count == 0)
                    continue;

                if (maxSignaturesCount < structure.Signatures.Count)
                {
                    maxSignaturesCount = structure.Signatures.Count;
                }

                structureIndex++;

                appendMacroLine("STRUCTURES_NAMES", $"    structenum_{structure.Name}={structureIndex},");
                appendMacroLine("STRUCTURES_INITIALIZE_NAMES", $"    \"{structure.Name}\",");
                appendMacroLine("STRUCTURES_DECLARATION", $"PyAPI_DATA({structure.Type}) {structure.Name} = 0;");
                appendMacroLine("STRUCTURES_FILL_TRAMPOLINES",  $"    {structure.Name}=({structure.Type}*)xfwnative_autogen_struct_addrs[structenum_{structure.Name}];");

                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_COUNT", $"    {structure.Signatures.Count},");

                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_FUNCTIONINDEX", "    {");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_OFFSET", "    {");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKDATA", "    {");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKMASK", "    {");


                foreach (var sig in structure.Signatures)
                {
                    var tokens = sig.Signature.Split('+');

                    appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_FUNCTIONINDEX", $"        funcenum_{tokens[0]},");
                    appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_OFFSET", $"        {tokens[1]},");

                    (var pattern, var mask) = Helper.ProcessSignature(sig.ValidationPrefix);
                    appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKDATA", $"        \"{pattern}\",");
                    appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKMASK", $"        \"{mask}\",");
                }

                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_FUNCTIONINDEX", "    },");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_OFFSET", "    },");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKDATA", "    },");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKMASK", "    },");
            }

            appendMacro("STRUCTURES_SIGNATURES_MAXCOUNT", maxSignaturesCount.ToString());
            appendMacro("STRUCTURES_COUNT", (structureIndex + 1).ToString());
        }

        public void Generate()
        {
            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }

            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(Path.Combine(outputDir));
            }

            foreach (var file in Directory.GetFiles(templateDir))
            {
                var fileContent = File.ReadAllText(file);
                foreach (var macro in macroses)
                {
                    fileContent = fileContent.Replace($"<<{macro.Key}>>", macro.Value.ToString());
                }


                File.WriteAllText(Path.Combine(outputDir,Path.GetFileNameWithoutExtension(file)),fileContent);
            }
        }

    }
}
