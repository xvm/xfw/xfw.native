﻿namespace XFWNative.Libpython.Common.Data.Signatures
{
    public class StructureSignature
    {
        public string Signature { get; set; }
        public string ValidationPrefix { get; set; }
    }
}
