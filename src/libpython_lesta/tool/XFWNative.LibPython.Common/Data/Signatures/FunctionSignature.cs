﻿namespace XFWNative.Libpython.Common.Data.Signatures
{
    public class FunctionSignature
    {
        public string Signature { get; set; }
    }
}
