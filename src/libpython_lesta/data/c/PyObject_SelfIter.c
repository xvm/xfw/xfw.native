#define Py_BUILD_CORE 1
#include "Python.h"


//CPython 2.7.18, object.c, L#1308
PyObject *
PyObject_SelfIter(PyObject *obj)
{
    Py_INCREF(obj);
    return obj;
}