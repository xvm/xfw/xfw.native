#define Py_BUILD_CORE 1

#include "Python.h"

PyAPI_FUNC(void) PySys_AddWarnOption(char *);

extern PyObject** warnoptions;

void
PySys_AddWarnOption(char *s)
{
    PyObject *str;

    if (*warnoptions == NULL || !PyList_Check(*warnoptions)) {
        Py_XDECREF(*warnoptions);
        *warnoptions = PyList_New(0);
        if (*warnoptions == NULL)
            return;
    }
    str = PyString_FromString(s);
    if (str != NULL) {
        PyList_Append(*warnoptions, str);
        Py_DECREF(str);
    }
}