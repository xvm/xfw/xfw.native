#define Py_BUILD_CORE 1

#include "Python.h"

PyAPI_FUNC(void) PySys_ResetWarnOptions(void);

extern PyObject** warnoptions;

void
PySys_ResetWarnOptions(void)
{
    if ((*warnoptions) == NULL || !PyList_Check(*warnoptions))
        return;

    PyList_SetSlice(*warnoptions, 0, PyList_GET_SIZE(*warnoptions), NULL);
}
