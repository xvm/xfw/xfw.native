#define Py_BUILD_CORE 1
#include "Python.h"

#undef PyFunction_GetCode
PyAPI_FUNC(PyObject *) PyFunction_GetCode(PyObject *);

PyObject *
PyFunction_GetCode(PyObject *op)
{
    if (!PyFunction_Check(op)) {
        PyErr_BadInternalCall();
        return NULL;
    }
    return ((PyFunctionObject *) op) -> func_code;
}