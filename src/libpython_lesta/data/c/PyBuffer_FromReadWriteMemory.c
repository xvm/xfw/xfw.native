#define Py_BUILD_CORE 1
#include "Python.h"

//CPython 2.7.18, bufferobject.c, L#7
typedef struct {
    PyObject_HEAD
        PyObject* b_base;
    void* b_ptr;
    Py_ssize_t b_size;
    Py_ssize_t b_offset;
    int b_readonly;
    long b_hash;
} PyBufferObject;


//CPython 2.7.18, bufferobject.c, L#98
static PyObject*
buffer_from_memory(PyObject* base, Py_ssize_t size, Py_ssize_t offset, void* ptr,
    int readonly)
{
    PyBufferObject* b;

    if (size < 0 && size != Py_END_OF_BUFFER) {
        PyErr_SetString(PyExc_ValueError,
            "size must be zero or positive");
        return NULL;
    }
    if (offset < 0) {
        PyErr_SetString(PyExc_ValueError,
            "offset must be zero or positive");
        return NULL;
    }

    b = PyObject_NEW(PyBufferObject, &PyBuffer_Type);
    if (b == NULL)
        return NULL;

    Py_XINCREF(base);
    b->b_base = base;
    b->b_ptr = ptr;
    b->b_size = size;
    b->b_offset = offset;
    b->b_readonly = readonly;
    b->b_hash = -1;

    return (PyObject*)b;
}

//CPython 2.7.18, bufferobject.c, L#193
PyObject*
PyBuffer_FromReadWriteMemory(void* ptr, Py_ssize_t size)
{
    return buffer_from_memory(NULL, size, 0, ptr, 0);
}