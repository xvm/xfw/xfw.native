#define Py_BUILD_CORE 1
#include "Python.h"


//CPython 2.7.18, exceptions.c, L#1310
static int
set_string(PyObject **attr, const char *value)
{
    PyObject *obj = PyString_FromString(value);
    if (!obj)
        return -1;
    Py_XSETREF(*attr, obj);
    return 0;
}


//CPython 2.7.18, exceptions.c, L#1526
int
PyUnicodeEncodeError_SetReason(PyObject *exc, const char *reason)
{
    return set_string(&((PyUnicodeErrorObject *)exc)->reason, reason);
}
